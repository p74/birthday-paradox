# -----------------------------------------------------------
# A demonstration on the birthday paradox
#
# Description:
# In probability theory, the birthday problem
# or birthday paradox concerns the probability
# that, in a set of n randomly chosen people,
# some pair of them will have the same birthday.
# Surprisingly, the probability of finding at
# least one pair in a set of 23 random chosen
# people is almost 50%. 
#
# More info on the problem:
# https://en.wikipedia.org/wiki/Birthday_problem
#
# Copyright © 2020 Pouyan Jaberi
# Released under MIT License
# Email:   pouyan.j@pm.me
# Website: https://p74.ir
# -----------------------------------------------------------
from random import randint
from datetime import date

class Person:
    """For each individual to store birthday

    """
    def __init__(self, birthday, birthmonth):
        self.birthday = birthday
        self.birthmonth = birthmonth


class Birthday_Paradox(object):
    """A toolbox of functions to simulate the birthday paradox

    This class contains static functions to demonstrate the birthday
    paradox, including functions to generate random birthdays and
    random populations and finally calculating the probability
    of finding matching cases (persons with same birthdays).

    Examples:
        >>> print('Probability ≈', Birthday_Paradox.calculate_probability())
        Probability ≈ 0.512

        >>> print('Probability ≈', Birthday_Paradox.calculate_probability(count=10000, population_size=32, leap_year=True))
        Probability ≈ 0.7499

    """

    @staticmethod
    def gen_ran_date(leap_year: bool = False) -> date:
        """Generates a random date

        Agrs:
            leap_year (bool): if enabled, 29th of Feb. would also be acceptable.
                (default is False)

        Returns:
            datetime.date: a randomly generated date
                from (1, 1, 1) to (9999, 12, 31)

        """
        start_dt = date.min.toordinal() # datetime.date(1, 1, 1)
        end_dt = date.max.toordinal() # datetime.date(9999, 12, 31)
        ran_dt = date.fromordinal(randint(start_dt, end_dt)) # randomly generated date
        if not leap_year: # if leap year is deactivated
            if ran_dt.day == 29 and ran_dt.month == 2: # check for Feb. 29
                ran_dt = Birthday_Paradox.gen_ran_date() # replace with a new date
        return ran_dt

    @staticmethod
    def initialize_random_population(population_size: int = 23, leap_year: bool = False) -> list:
        """Creates a list of ``Person`` objects

        Args:
            population_size (int): The number of required ``Person`` objects
                (default is 23)
            leap_year (bool): if enabled, 29th of Feb. would also be acceptable.
                (default is False)

        Returns:
            list: a list randomly generated ``Person`` objects at the size
            of ``population_size``

        """
        persons = list() # initialization
        for _ in range(population_size):
            birthdate = Birthday_Paradox.gen_ran_date(leap_year) # generating random date
            persons.append(Person(birthdate.day, birthdate.month)) # appending new object
        return persons
    
    @staticmethod
    def pair_found(persons: list = None) -> bool:
        """Checks if at least one birthday match is found

        Args:
            persons (list): The list of persons to scan for possible matches

        Returns:
            bool: True if at least one matching pair is found, False if not

        """
        if persons is None:
            persons = list() # creating an empty list, in case of no input

        for person in persons: # scanning through the list
            for i in range(persons.index(person) + 1, len(persons)): # comparing each person to the following ones
                if person.birthday == persons[i].birthday and person.birthmonth == persons[i].birthmonth: # if pair found
                    return True # match found
        return False # no match found

    @staticmethod
    def gen_ran_populations(count: int = 1000, population_size: int = 23, leap_year: bool = False) -> list:
        """Generates random populations in given dimensions

        Args:
            count (int): Absolute number of population lists (default is 1000)
            population_size (int): The size of each population (default is 23)
            leap_year (bool): if enabled, 29th of Feb. would also be acceptable.
                (default is False)

        Returns:
            list: A list, containing lists of ``Person`` objects

        """
        populations = list() # initialization
        for _ in range(count):
            # appending new generated populations to the list
            populations.append(Birthday_Paradox.initialize_random_population(population_size, leap_year))
        return populations

    @staticmethod
    def calculate_probability(count: int = 1000, population_size: int = 23, leap_year: bool = False) -> float:
        """Calculates the average number of lists with matching persons

        Generates a ``count``-big list of populations, each containing ``population_size``
        objects of ``Person``. Then counts the lists with at least one pair with matching
        birthday and divides it by the total number of lists (which is ``count``).

        Args:
            count (int): Absolute number of population lists (default is 1000)
            population_size (int): The size of each population (default is 23)

        Returns:
            float: A list, containing lists of ``Person`` objects

        """
        populations = Birthday_Paradox.gen_ran_populations(count, population_size, leap_year)
        pair_found_count = 0 # initialization
        for population in populations: # checking each population set
            if Birthday_Paradox.pair_found(population): # checking possible matches
                pair_found_count += 1 # increments by one to count the total number of lists with positivity on ``pair_found``
        return pair_found_count / count # returns the average 

def main():
    total_runs = 100
    sum = 0
    for _ in range(total_runs):
        sum += Birthday_Paradox.calculate_probability(count=1000, population_size=23, leap_year=False)
    print(f'Average probability on {total_runs} runs ≈ {sum / total_runs}')

if __name__ == '__main__':
    main()